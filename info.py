__author__ = 'Pyro'

from tkinter import *

class Info:
    def __init__(self, frame, ycor, name, file="data/info/AC.txt"):
        self.filename = file
        self.ycor = ycor
        self.name = name
        self.frame = frame

        self.infoIcon = PhotoImage(file="img/info.gif")
        self.info = Button(self.frame, image=self.infoIcon, command=self.press)
        self.info.place(x=360, y=ycor-0.25)

    def press(self):
        print("Click!")