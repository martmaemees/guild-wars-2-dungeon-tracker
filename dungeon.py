__author__ = 'Pyro'

from tkinter import *
import time
import stats
import info
import os.path

class Dungeon:
    def __init__(self,frame, ycor, name, file, token, gold, infoFile):
        self.filename = file
        self.name = name
        self.ycor = ycor
        self.nameLabel = Label(frame, text=name, font="Helvetica 10 bold", fg="darkred", bg="lightgrey")
        self.buttonS = Button(frame, text="S", command=self.story, width=3)
        self.button1 = Button(frame, text="P1", command=self.p1, width=3)
        self.button2 = Button(frame, text="P2", command=self.p2, width=3)
        self.button3 = Button(frame, text="P3", command=self.p3, width=3)
        if self.name == "The Ruined City of Arah":
            self.button4 = Button(frame, text="P4", command=self.p4, width=3)
        self.loadFile()
        self.date = time.strftime("%d/%m/%y")
        print(self.date)
        self.checkDone()
        #print(self.date)
        self.stats = stats.Stats(frame, ycor, name, self.dataStory, self.dataPath1, self.dataPath2, self.dataPath3,
                                 token, gold)

        # self.info = info.Info(frame, ycor, name, infoFile)
        if self.name == "The Ruined City of Arah":
            self.stats.setPath4(self.dataPath4)
        self.colorName()

    def colorName(self):
        if self.name == "The Ruined City of Arah":
            x = 0
            if len(self.dataPath1) != 0:
                if self.dataPath1[0] == self.date:
                    x+=1
            if len(self.dataPath2) != 0:
                if self.dataPath2[0] == self.date:
                    x+=1
            if len(self.dataPath3) != 0:
                if self.dataPath3[0] == self.date:
                    x+=1
            if len(self.dataPath4) != 0:
                if self.dataPath4[0] == self.date:
                    x+=1
            if x==4:
                self.nameLabel.configure(fg="green")
            elif x > 0:
                self.nameLabel.configure(fg="orange")
            else:
                self.nameLabel.configure(fg="darkred")
        else:

            x = 0
            if len(self.dataPath1) != 0:
                if self.dataPath1[0] == self.date:
                    x+=1
            if len(self.dataPath2) != 0:
                if self.dataPath2[0] == self.date:
                    x+=1
            if len(self.dataPath3) != 0:
                if self.dataPath3[0] == self.date:
                    x+=1
            if x==3:
                self.nameLabel.configure(fg="green")
            elif x > 0:
                self.nameLabel.configure(fg="orange")
            else:
                self.nameLabel.configure(fg="darkred")


    def saveFile(self):
        f = open(self.filename, "w")
        f.write("Story\n")
        for i in self.dataStory:
            f.write(i+"\n")
        f.write("Path1\n")
        for i in self.dataPath1:
            f.write(i+"\n")
        f.write("Path2\n")
        for i in self.dataPath2:
            f.write(i+"\n")
        f.write("Path3\n")
        for i in self.dataPath3:
            f.write(i+"\n")
        if self.name == "The Ruined City of Arah":
            f.write("Path4\n")
            for i in self.dataPath4:
                f.write(i+"\n")

    def checkDone(self):
        if self.dataStory.count(self.date) > 0:
            self.buttonS.configure(relief="sunken")
        if self.dataPath1.count(self.date) > 0:
            self.button1.configure(relief="sunken")
        if self.dataPath2.count(self.date) > 0:
            self.button2.configure(relief="sunken")
        if self.dataPath3.count(self.date) > 0:
            self.button3.configure(relief="sunken")
        if self.name=="The Ruined City of Arah":
            if self.dataPath4.count(self.date) > 0:
                self.button4.configure(relief="sunken")

    def loadFile(self):

        if not os.path.exists('data'):
            os.mkdir('data')

        dungeons = ['data/AC.txt', 'data/Arah.txt', 'data/CM.txt', 'data/CoE.txt', 'data/CoF.txt', 'data/HotW.txt', 'data/SE.txt', 'data/TA.txt']
        for dung in dungeons:
            print(dung)
            if not os.path.isfile(dung):
                f = open(dung, 'w+')
                f.write('Story\n')
                f.write('Path1\n')
                f.write('Path2\n')
                f.write('Path3\n')
                if dung == '/data/Arah.txt':
                    f.write('Path4\n')

        self.dataStory = []
        self.dataPath1 = []
        self.dataPath2 = []
        self.dataPath3 = []
        if self.name == "The Ruined City of Arah":
            self.dataPath4 = []
        f = open(self.filename)
        f.readline()
        for i in f:
            i = i.strip()
            if i=="Path1":
                break
            self.dataStory.append(i)
        for i in f:
            i = i.strip()
            if i=="Path2":
                break
            self.dataPath1.append(i)
        for i in f:
            i = i.strip()
            if i=="Path3":
                break
            self.dataPath2.append(i)
        if self.name == "The Ruined City of Arah":
            for i in f:
                i = i.strip()
                if i=="Path4":
                    break
                self.dataPath3.append(i)
        for i in f:
            i = i.strip()
            if self.name == "The Ruined City of Arah":
                self.dataPath4.append(i)
            else:
                self.dataPath3.append(i)
        """ # Output current data
        print(self.dataStory,"\n",self.dataPath1,"\n",self.dataPath2,"\n",self.dataPath3)
        if self.name == "The Ruined City of Arah":
            print(self.dataPath4)
        """
        f.close()

    def activateStats(self):
        self.stats.calculate()
        self.stats.placeGold()
        self.stats.placeSilver()
        self.stats.placeXP()
        self.stats.placeToken()
        self.buttonS.place_forget()
        self.button1.place_forget()
        self.button2.place_forget()
        self.button3.place_forget()
        if self.name == "The Ruined City of Arah":
            self.button4.place_forget()

    def deactivateStats(self):
        self.stats.forgetAll()

    def placeName(self, xcor=10):
        self.nameLabel.place(x=xcor, y=self.ycor)

    def placePath1(self, xcor=205):
        self.button1.place(x=xcor, y=self.ycor-1)

    def placePath2(self, xcor=240):
        self.button2.place(x=xcor, y=self.ycor-1)

    def placePath3(self, xcor=275):
        self.button3.place(x=xcor, y=self.ycor-1)

    def placePath4(self, xcor=310):
        self.button4.place(x=xcor, y=self.ycor-1)

    def placeStory(self, xcor=170):
        self.buttonS.place(x=xcor, y=self.ycor-1)

    def p1(self):
        #print("Click!")
        if self.button1["relief"] == "raised":
            self.button1.configure(relief="sunken")
            self.dataPath1.reverse()
            self.dataPath1.append(self.date)
            self.dataPath1.reverse()
        else:
            self.button1.configure(relief="raised")
            self.dataPath1.remove(self.date)
        self.saveFile()
        self.colorName()

    def p2(self):
        #print("Click!")
        if self.button2["relief"] == "raised":
            self.button2.configure(relief="sunken")
            self.dataPath2.reverse()
            self.dataPath2.append(self.date)
            self.dataPath2.reverse()
        else:
            self.button2.configure(relief="raised")
            self.dataPath2.remove(self.date)
        self.saveFile()
        self.colorName()

    def p3(self):
        #print("Click!")
        if self.button3["relief"] == "raised":
            self.button3.configure(relief="sunken")
            self.dataPath3.reverse()
            self.dataPath3.append(self.date)
            self.dataPath3.reverse()
        else:
            self.button3.configure(relief="raised")
            self.dataPath3.remove(self.date)
        self.saveFile()
        self.colorName()

    def p4(self):
        #print("Click!")
        if self.button4["relief"] == "raised":
            self.button4.configure(relief="sunken")
            self.dataPath4.reverse()
            self.dataPath4.append(self.date)
            self.dataPath4.reverse()
        else:
            self.button4.configure(relief="raised")
            self.dataPath4.remove(self.date)
        self.saveFile()
        self.colorName()

    def story(self):
        #print("Click!")
        if self.buttonS["relief"] == "raised":
            self.buttonS.configure(relief="sunken")
            self.dataStory.reverse()
            self.dataStory.append(self.date)
            self.dataStory.reverse()
        else:
            self.buttonS.configure(relief="raised")
            self.dataStory.remove(self.date)
        self.saveFile()