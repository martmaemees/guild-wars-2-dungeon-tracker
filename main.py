__author__ = 'Pyro'

from tkinter import *
import dungeon
import stats

def placeButtons(x):
    x.placePath1()
    x.placePath2()
    x.placePath3()
    if x==Arah:
        x.placePath4()
    x.placeStory()

def placeAllButtons():
    placeButtons(AC)
    placeButtons(CM)
    placeButtons(TA)
    placeButtons(SE)
    placeButtons(CoF)
    placeButtons(HotW)
    placeButtons(CoE)
    placeButtons(Arah)

def stats():
    if Stats["relief"] == "raised":
        AC.activateStats()
        CM.activateStats()
        TA.activateStats()
        SE.activateStats()
        CoF.activateStats()
        HotW.activateStats()
        CoE.activateStats()
        Arah.activateStats()
        Stats.configure(relief="sunken")
    else:
        AC.deactivateStats()
        CM.deactivateStats()
        TA.deactivateStats()
        SE.deactivateStats()
        CoF.deactivateStats()
        HotW.deactivateStats()
        CoE.deactivateStats()
        Arah.deactivateStats()
        placeAllButtons()
        Stats.configure(relief="raised")

raam = Tk()
raam.title("Dungeons")
raam.geometry("420x300")
raam.configure(bg="lightgrey")

AC = dungeon.Dungeon(raam, 10, "Ascalonian Catacombs", "data/AC.txt", "img/AC_token.gif", 1.81, "data/info/AC.txt")
AC.placeName()
CM = dungeon.Dungeon(raam, 40, "Caudecus's Manor", "data/CM.txt", "img/CM_token.gif", 1.31, "data/info/AC.txt")
CM.placeName()
TA = dungeon.Dungeon(raam, 70, "Twilight Arbor", "data/TA.txt", "img/TA_token.gif", 2.31, "data/info/AC.txt")
TA.placeName()
SE = dungeon.Dungeon(raam, 100, "Sorrow's Embrace", "data/SE.txt", "img/SE_token.gif", 1.31, "data/info/AC.txt")
SE.placeName()
CoF = dungeon.Dungeon(raam, 130, "Citadel of Flame", "data/CoF.txt", "img/CoF_token.gif", 1.31, "data/info/AC.txt")
CoF.placeName()
HotW = dungeon.Dungeon(raam, 160, "Honor of the Waves", "data/HotW.txt", "img/HotW_token.gif", 1.31, "data/info/AC.txt")
HotW.placeName()
CoE = dungeon.Dungeon(raam, 190, "Crucible of Eternity", "data/CoE.txt", "img/CoE_token.gif", 1.31, "data/info/AC.txt")
CoE.placeName()
Arah = dungeon.Dungeon(raam, 220, "The Ruined City of Arah", "data/Arah.txt", "img/Arah_token.gif", 3.31, "data/info/AC.txt")
Arah.placeName()
placeAllButtons()

Stats = Button(raam, text="Stats", command=stats, font="Helvetica 11 bold", fg="darkblue", width=10)
Stats.place(x=10, y=260)

raam.mainloop()