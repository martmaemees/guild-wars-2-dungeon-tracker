__author__ = 'Pyro'

from tkinter import *

class Stats:
    def __init__(self, frame, ycor, name, dataS, data1, data2, data3, img, gold):
        self.name = name
        self.ycor = ycor
        self.dataS = len(dataS)
        self.path1 = data1
        self.path2 = data2
        self.path3 = data3
        self.img = img
        self.money = gold

        self.tokenLogo = PhotoImage(file=img)
        self.tokenLabel = Label(frame, image=self.tokenLogo, bg="lightgrey")
        self.xpLogo = PhotoImage(file="img/XP.gif")
        self.xpLabel = Label(frame, image=self.xpLogo, bg="lightgrey")
        self.gLogo = PhotoImage(file="img/Gold_coin.gif")
        self.gLabel = Label(frame, image=self.gLogo, bg="lightgrey")
        self.sLogo = PhotoImage(file="img/Silver_coin.gif")
        self.sLabel = Label(frame, image=self.sLogo, bg="lightgrey")

        self.gText = Label(frame, text=90000, bg="lightgrey")
        self.sText = Label(frame, text=99, bg="lightgrey")
        self.xpText = Label(frame, text=4444, bg="lightgrey")
        self.tokenText = Label(frame, text=5555, bg="lightgrey")

    def calculate(self):
        if self.money == 2.31:
            self.gold = (len(self.path1)+len(self.path2))*1.31 + len(self.path3)*2.31
        elif self.money == 3.31:
            self.gold = (len(self.path1)+len(self.path2)+len(self.path4))*3.31 + len(self.path3)*1.81
        else:
            self.gold = (len(self.path1)+len(self.path2)+len(self.path3))*self.money
        self.money += self.dataS*0.73
        self.silver = int((self.gold-int(self.gold))*100)
        self.gold = int(self.gold)

        self.tokens = (len(self.path1)+len(self.path2)+len(self.path3))*60
        self.xp = (len(self.path1)+len(self.path2)+len(self.path3)+self.dataS)*0.75
        if self.name == "The Ruined City of Arah":
            self.xp += len(self.path4)*0.75

        self.gText.configure(text=self.gold)
        self.sText.configure(text=self.silver)
        self.tokenText.configure(text=self.tokens)
        self.xpText.configure(text=self.xp)

    def placeGold(self, xcor=165):
        self.gText.place(x=xcor, y=self.ycor+1)
        self.gLabel.place(x=xcor+35, y=self.ycor+1)

    def placeSilver(self, xcor=220):
        self.sText.place(x=xcor, y=self.ycor+1)
        self.sLabel.place(x=xcor+20, y=self.ycor+1)

    def placeXP(self, xcor=320):
        self.xpText.place(x=xcor, y=self.ycor+1)
        self.xpLabel.place(x=xcor+30, y=self.ycor+1)

    def placeToken(self, xcor=265):
        self.tokenText.place(x=xcor, y=self.ycor+1)
        self.tokenLabel.place(x=xcor+30, y=self.ycor-1)

    def forgetAll(self):
        self.gText.place_forget()
        self.gLabel.place_forget()
        self.sText.place_forget()
        self.sLabel.place_forget()
        self.xpText.place_forget()
        self.xpLabel.place_forget()
        self.tokenLabel.place_forget()
        self.tokenText.place_forget()

    def setPath4(self, data):
        self.path4 = data