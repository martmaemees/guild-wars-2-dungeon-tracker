# README #


```
#!shell

Requires Python 3 to run.

Run from main.py
```


### What is this repository for? ###

Simple UI that lets you check off the paths of Guild Wars 2 dungeons you have finished on that day. The dungeon name turns green when all the exploration paths are checked off on that day. Resets when you restart the application after midnight your computer time. The application saves the date for every path you check off and displays the earned gold, dungeon specific currency and character levels you have earned for each path during the usage of this application.



**I have halted any work on this application for now due to not playing any Guild Wars 2 anymore.**